package ru.user0dev;

import com.jcraft.jsch.*;
import org.apache.commons.cli.*;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import static java.nio.file.attribute.PosixFilePermission.*;


public class MassUploader {

    private static final String USAGE = "java -jar massuploader.jar [OPTIONS] files destination";

    private static int CONNECTION_TIMEOUT = 10000; //timeout in milliseconds

    private static final String ERROR_WRONG_ARGS = "Wrong argument";

    private static final String[] STD_KEYS = {".ssh/id_dsa", ".ssh/id_ecdsa", ".ssh/id_ed25519", ".ssh/id_rsa"};

    private enum OPTS {
        hosts, serversfile, port, identity, password, login, help
    }

    private static final Options OPTIONS = new Options() {{
        addOption(Option.builder("H").longOpt("" + OPTS.hosts).hasArg().desc("Comma-separated list of servers for uploading files").build());
        addOption(Option.builder("s").longOpt("" + OPTS.serversfile).hasArg().desc("path to the file with addresses").build());
        addOption(Option.builder("P").longOpt("" + OPTS.port).hasArg().argName("port").desc("specifies the port to connect to on the remote hosts").build());
        addOption(Option.builder("i").longOpt("" + OPTS.identity).argName("path").hasArg().desc("path to private key for public key authentication").build());
        addOption(Option.builder("p").longOpt("" + OPTS.password).hasArg().desc("Password to use when connecting to server. If password is not given it's asked from the tty.").build());
        addOption(Option.builder("l").longOpt("" + OPTS.login).hasArg().argName("name").desc("The user name to use when connecting to the server.").build());
        addOption(Option.builder("h").longOpt("" + OPTS.help).desc("print this help").build());
    }};

    private static JSch jsch = new JSch();
    private static HelpFormatter helpFormatter = new HelpFormatter();
    private static CommandLine cmd;


    private static List<Path> uploadFiles = new ArrayList<>();
    private static String destination = "";

    private static int port = 22;
    private static String user = System.getProperty("user.name");
    private static String key = "";
    private static String password = "";
    private static List<String> hosts = new ArrayList<>();



    private static void printHelp() {
        helpFormatter.printHelp(USAGE, OPTIONS);
    }

    private static void optionProcessing(CommandLine cmd) throws IOException {
        if (cmd.hasOption('h')) {
            printHelp();
            throw new ExitException("", false);
        }

        String[] arguments = cmd.getArgs();

        if (arguments.length < 2) {
            printHelp();
            throw new ExitException("Missed argument. You must specify file from download and destination");
        }

        for (int i = 0; i < arguments.length - 1; i++) {
            Path fsEntity =  Paths.get(arguments[i]);
            if (Files.notExists(fsEntity)) {
                //throw new ExitException(fsEntity.toString() + " isn't exists");
                uploadFiles.add(fsEntity);
            } else if (Files.isRegularFile(fsEntity) || Files.isDirectory(fsEntity)) {
                uploadFiles.add(fsEntity);
//            } else if (Files.isDirectory(fsEntity)) {
//                throw new ExitException("Uploading directory is not supported");
            } else if (Files.isSymbolicLink(fsEntity)) {
                throw new ExitException("Uploading symbolic links is not supported");
            }
        }

        if (uploadFiles.size() == 0) {
            printHelp();
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for (int i = 0; i < arguments.length -1; i++) {
                sb.append(arguments[i]);
                if (i < arguments.length - 2) {
                    sb.append(", ");
                }
            }
            sb.append("]");
            throw new ExitException("Wrong uploading file names: " + sb);
        }

        String rawDestination = arguments[arguments.length - 1];
        if (rawDestination.trim().isEmpty()) {
            throw new ExitException("Wrong destination path: " + rawDestination);
        } else {
            destination = rawDestination;
            if (!destination.endsWith("/")) {
                destination += "/";
            }
        }


        if (cmd.hasOption("" + OPTS.port)) {
            String portRawStr = cmd.getOptionValue("" + OPTS.port);
            try {

                int portRaw = Integer.parseInt(portRawStr);
                if (portRaw < 1 || portRaw > 65535) {
                    throw new NumberFormatException();
                }
                port = portRaw;
            } catch (NumberFormatException ex) {
                throw new ExitException("Wrong argument. Port cannot be " + portRawStr);
            }
        }

        if (cmd.hasOption("" + OPTS.login)) {
            String rawUser = cmd.getOptionValue("" + OPTS.login);
            if (!Pattern.compile("\\w+").matcher(rawUser).matches()) {
                throw new ExitException("Wrong user name: " + rawUser);
            }
            user = rawUser;
        }

        if (cmd.hasOption("" + OPTS.password)) {
            password = cmd.getOptionValue("" + OPTS.password);
        }

        if (cmd.hasOption("" + OPTS.identity)) {
            String rawKey = cmd.getOptionValue("" + OPTS.identity);
            if (new File(rawKey).isFile()) {
                key = rawKey;
            } else {
                throw new ExitException("Wrong private key: " + rawKey);
            }
        }

        if (cmd.hasOption("" + OPTS.hosts)) {
            String rawHostsStr = cmd.getOptionValue("" + OPTS.hosts);
            String[] rawHostArr = rawHostsStr.split(",");
            for (String rh : rawHostArr) {
                if (rh.trim().isEmpty()) {
                    throw new ExitException("Wrong hosts values: " + rawHostsStr);
                }
                hosts.add(rh);
            }
        }
        String rawServerFileStr = cmd.getOptionValue("" + OPTS.serversfile);
        if (rawServerFileStr != null) {
            Path rawServerFile = Paths.get(rawServerFileStr);
            try {
                Files.readAllLines(rawServerFile).stream().filter(s -> !s.isEmpty()).collect(Collectors.toCollection(() -> hosts));

            } catch (IOException ex) {
                throw new ExitException("Error during read file: " + rawServerFileStr + "Error: " + ex.getMessage(), ex);
            }
        }




        if (hosts.size() == 0) {
            throw new ExitException("You must specify options -l or -H with server's address");
        }
    }

    private static class ExitException extends RuntimeException {

        private boolean error;

        public boolean isError() {
            return error;
        }

        public ExitException(String message, boolean error) {
            super(message);
            this.error = error;
        }
        public ExitException(String message) {
            super(message);
            error = true;
        }
        public ExitException(String message, Throwable cause) {
            super(message, cause);
            error = true;
        }
    }

    private static void debugPrintParameters() {
        System.out.printf("User: %s%nPassword: %s%nKey: %s%nPort: %d%nServers: %s%nFiles: %s%nDestination: %s%n", user, password, key, port, hosts, uploadFiles, destination);
    }

    private static void loadKeys() throws JSchException {
        if (!key.isEmpty()) {
            jsch.addIdentity(key);
        }
        String home = System.getProperty("user.home") + "/";
        for (String stdKey : STD_KEYS) {
            if (Files.exists(Paths.get(home + stdKey))) {
                jsch.addIdentity(home + stdKey);
            }
        }
    }

    //не работает
//    private static boolean dropThread(String host) {
//
//        String dropedName = "Opening Socket " + host;
//
//        Set<Thread> threads = Thread.getAllStackTraces().keySet();
//        for (Thread thread : threads) {
//            if (thread.getName().equals(dropedName)) {
//                System.err.println("Drop thread: " + dropedName);
//                thread.stop();
//            }
//        }
//        return false;
//    }

    private static boolean isJschThreadsHang() {
        String findName = "Opening Socket ";
        for (Thread thread : Thread.getAllStackTraces().keySet()) {
            if (thread.getName().startsWith(findName)) {
                return true;
            }
        }
        return false;
    }
    private static int javaPosixAttrToBits(Set<PosixFilePermission> attr) {

        int bits = 0;
        if (attr.contains(OTHERS_EXECUTE)) {
            bits |= 1;
        }
        if (attr.contains(OTHERS_WRITE)) {
            bits |= 2;
        }
        if (attr.contains(OTHERS_READ)) {
            bits |= 4;
        }
        if (attr.contains(GROUP_EXECUTE)) {
            bits |= 010;
        }
        if (attr.contains(GROUP_WRITE)) {
            bits |= 020;
        }
        if (attr.contains(GROUP_READ)) {
            bits |= 040;
        }
        if (attr.contains(OWNER_EXECUTE)) {
            bits |= 0100;
        }
        if (attr.contains(OWNER_WRITE)) {
            bits |= 0200;
        }
        if (attr.contains(OWNER_READ)) {
            bits |= 0400;
        }
        return bits;
    }

    public static void main(String[] args) throws IOException, JSchException, SftpException {
        try {
            CommandLineParser parser = new DefaultParser();
            try {
                cmd = parser.parse(OPTIONS, args);
            } catch (ParseException ex) {
                throw new ExitException("Wrong options: " + ex.getMessage());
            }

            optionProcessing(cmd);
//            debugPrintParameters();

            loadKeys();
            for (final String host : hosts) {
                try { // для обработки зависания jsch
                    Session session = null;
                    try { // for close jsch 'Session session'
                        session = jsch.getSession(user, host, port);
                        session.setConfig("StrictHostKeyChecking", "no");
                        session.connect(CONNECTION_TIMEOUT);
                        final ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");;
                        try { //for close final ChannelSftp sftp
                            sftp.connect(CONNECTION_TIMEOUT);
                            try {
                                SftpATTRS attr = sftp.stat(destination);
                                if (!attr.isDir()) {
                                    throw new ExitException(String.format("Host: %s. Destination '%s' is not directory", host, destination));
                                }
                            } catch (SftpException ex) {
                                if (ex.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
                                    System.out.printf("Mkdir '%s' on '%s'%n", destination, host);
                                    sftp.mkdir(destination);
                                }
                            }

                            for (Path file : uploadFiles) {
                                if (Files.notExists(file)) {
                                    throw new ExitException("Not exists file: " + file);
                                } else if (Files.isRegularFile(file)) {
                                    String fullPath = file.toAbsolutePath().toString();
                                    System.out.printf("Upload file '%s' on '%s' to '%s'%n", fullPath, host, destination);
                                    sftp.put(fullPath, destination);
                                    String remoteName = destination + file.getFileName().toString();
                                    SftpATTRS remoteAttr = sftp.stat(remoteName);
                                    PosixFileAttributes localAttr = Files.readAttributes(file, PosixFileAttributes.class);
                                    remoteAttr.setPERMISSIONS(javaPosixAttrToBits(localAttr.permissions()));
                                    sftp.setStat(remoteName, remoteAttr);
                                } else if (Files.isDirectory(file)) {
                                    final Path parent = file.getParent();
                                    Files.walkFileTree(file, new SimpleFileVisitor<Path>() {
                                        @Override
                                        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                                            String remoteDir = destination + parent.relativize(dir).toString();
                                            System.out.printf("Mkdir '%s' on '%s'%n", remoteDir, host);
                                            try {
                                                sftp.mkdir(remoteDir);
                                                SftpATTRS remoteAttr = sftp.stat(remoteDir);
                                                remoteAttr.setPERMISSIONS(javaPosixAttrToBits(Files.readAttributes(dir, PosixFileAttributes.class).permissions()));
                                                sftp.setStat(remoteDir, remoteAttr);
                                            } catch (SftpException ex) {
                                                if (ex.id != ChannelSftp.SSH_FX_FAILURE) {
                                                    throw new IOException(String.format("Error when mkdir '%s' on '%s'", remoteDir, host), ex);
                                                }
                                            }
                                            return FileVisitResult.CONTINUE;
                                        }

                                        @Override
                                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                                            String remoteFile = destination + parent.relativize(file).toString();
                                            String newDestination = Paths.get(remoteFile).getParent().toString();
                                            System.out.printf("Upload file '%s' to '%s' on '%s'%n", file, newDestination, host);
                                            try {
                                                sftp.put(file.toString(), remoteFile);
                                                SftpATTRS remoteAttr = sftp.stat(remoteFile);
                                                remoteAttr.setPERMISSIONS(javaPosixAttrToBits(Files.readAttributes(file, PosixFileAttributes.class).permissions()));
                                                sftp.setStat(remoteFile, remoteAttr);
                                            } catch (SftpException ex) {
                                                throw new IOException(String.format("Error when upload file '%s' to '%s' on '%s''", file, newDestination, host), ex);
                                            }
                                            return FileVisitResult.CONTINUE;
                                        }

                                        @Override
                                        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                                            return FileVisitResult.CONTINUE;
                                        }
                                    }); // Files.walkFileTree(file, new SimpleFileVisitor<Path>()
                                } else {
                                    throw new ExitException("Unsupported file type: " + file);
                                }
                            } // for (Path file : uploadFiles)
                        } finally {
                            sftp.exit();
                            sftp.disconnect();
                        } //for close final ChannelSftp sftp
                    } finally {
                        if (session != null) {
                            session.disconnect();
                        }
                    } // for close jcsh 'Session session'
                } catch (JSchException ex) {
                    if (isJschThreadsHang()) {
                        throw new ExitException(String.format("Kill the program because jsch hands%nHost: %s%nPort: %d", host, port), ex);
                    } else {
                        throw ex;
                    }
                } // нужно для обработки зависания jsch


            }


        } catch (ExitException ex) {

            if (ex.getMessage() != null && !ex.getMessage().isEmpty()) {
                (ex.isError() ? System.err : System.out).println(ex.getMessage());
            }
            if (ex.getCause() != null) {
                System.err.println(ex.getCause());
            }
            if (ex.isError()) {
                System.exit(1);
            }
            return;
        }
    }

}
